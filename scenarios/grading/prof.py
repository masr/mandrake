import mandrake
from configuration import config, grading, Prof

adapter = mandrake.Adapter(Prof, grading, config, color=mandrake.adapter.COLORS[0])
adapter.load_asl("prof.asl")

if __name__ == "__main__":
    print("Starting Prof...")
    adapter.start()
