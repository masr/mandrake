import asyncio
import logging
from mandrake import Adapter
from configuration import config, grading, TA

# logging.getLogger("mandrake").setLevel(logging.DEBUG)

adapter = Adapter(TA, grading, config)
adapter.load_asl("ta.asl")

if __name__ == "__main__":
    print("Starting TA...")
    adapter.start()
