import logging
from mandrake import Adapter, Remind
from configuration import config, logistics
from mandrake.statistics import stats_logger

from Logistics import Packer, Packed

adapter = Adapter(Packer, logistics, config)

logger = logging.getLogger("mandrake")
# logger.setLevel(logging.DEBUG)


@adapter.enabled(Packed)
async def pack(msg):
    msg["status"] = "packed"
    return msg


if __name__ == "__main__":
    logger.info("Starting Packer...")
    adapter.start()
